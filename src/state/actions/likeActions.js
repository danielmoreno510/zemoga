export const putLikesPrincipal = payload => ({
  type: 'PUT_LIKES_PRINCIPAL',
  payload
})

export const putLikes = (payload, id) => ({
  type: 'PUT_LIKES',
  payload,
  id
})