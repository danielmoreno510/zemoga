import card1 from "../../images/card1.png";
import card2 from "../../images/card2.png";
import card3 from "../../images/card3.png";
import card4 from "../../images/card4.png";

const initialState = {
  principal:{
    likes: 0,
    dislikes: 0,
  },
  items:[
    {
      id:1,
      image: card1,
      title: "Kanye West",
      month:'1 month ago in Entertaimend',
      text: "Vestibulum diam ante, portitor a odio eget, rhoncus neque. Aenean eu velit libero",
      likes: 10,
      dislikes: 5
    },
    {
      id:2,
      image: card2,
      title: "Mark Zuckerberg",
      month:'1 month ago in Entertaimend',
      text: "Vestibulum diam ante, portitor a odio eget, rhoncus neque. Aenean eu velit libero",
      likes: 2,
      dislikes: 4
    },
    {
      id:3,
      image: card3,
      title: "Cristina Fernández de Kirchner",
      month:'1 month ago in Entertaimend',
      text: "Vestibulum diam ante, portitor a odio eget, rhoncus neque. Aenean eu velit libero",
      likes: 3,
      dislikes: 6
    },
    {
      id:4,
      image: card4,
      title: "Malala Yousafzai",
      month:'1 month ago in Entertaimend',
      text: "Vestibulum diam ante, portitor a odio eget, rhoncus neque. Aenean eu velit libero",
      likes: 20,
      dislikes: 30
    }
  ]
};

// modify draft, it creates the next immutable state based on changes
export default (state = initialState, action) => {
  switch (action.type) {
    case "PUT_LIKES_PRINCIPAL":
      if(action.payload==='like'){
        state.principal.likes=state.principal.likes+1
      }
      if(action.payload==='dislike'){
        state.principal.dislikes=state.principal.dislikes+1
      }
      return state
    case "PUT_LIKES":
      for(let i=0;state.items.length>i;i++){
        if(state.items[i].id==action.id)
        if(action.payload==='like'){
          state.items[i].likes=state.items[i].likes+1
        }
        if(action.payload==='dislike'){
          state.items[i].dislikes=state.items[i].dislikes+1
        }
      }
      return state
    default:
      return state
  }
}
