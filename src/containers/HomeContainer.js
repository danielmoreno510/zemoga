import { connect } from "react-redux";
import Home from "../components/Home";
import { putLikesPrincipal, putLikes } from '../state/actions'

const mapStateToProps = state => {
  return {
    data: state.likesReducer.principal,
    items: state.likesReducer.items,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    likePrincipal: like => {
      dispatch(putLikesPrincipal(like))
    },
    like: (like, id) => {
      dispatch(putLikes(like, id))
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
