import React from "react";
import { render } from 'react-dom'
import "./index.css";
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from './state/reducers'
import App from "./App";

let store = createStore(rootReducer)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);


