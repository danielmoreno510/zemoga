import React from "react";
import ConfigRouter from "./components/ConfigRouter/";

function App() {
  return <ConfigRouter />;
}

export default App;
