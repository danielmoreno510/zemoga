import React, {useState} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Carousel from '../Carousel';
import Home from '../../containers/HomeContainer'
import Header from '../Header';
import "./styles.css";

const ConfigRouter = () => {
  const [carousel, setCarousel] = useState(true);
  const change = (value) => {
    setCarousel(value)
  }
  return (
    <Router>
      <div className='routerContainer'>
        {carousel && <Carousel />}
        <Header change={change} />
      </div>
      <Route exact path="/" component={Home}/>
    </Router>
  )
}

export default ConfigRouter;
