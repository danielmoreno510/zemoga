import React, {useState} from "react";
import up from "../../images/up.png";
import down from "../../images/down.png";
import "./styles.css";
import ResultSection from '../ResultSection'
import { percentage } from '../../Utils/percentageUtil'

const Card = ({data, like, dislike}) => {
  const { image, title, month, text } = data;
  const [vote, setVote] = useState(false);
  const [results, setResults] = useState(percentage(data.likes, data.dislikes));
  const calc=()=>{
    setResults(percentage(data.likes, data.dislikes))
  }
  return (
    <div
      className="cardPrincipal"
      style={{ backgroundImage: 'url("' + image + '")' }}
    >
      <div className="cardPrincipalContainer">
        <div className="cardTextTitle">{title}</div>
        <div>{month}</div>
        <div className="cardTexInfo">{text}</div>
        <div className='thumbSections'>
          <div className='thumbSection1' onClick={()=>{like(); calc()}}><img src={up} /></div>
          <div className='thumbSection2' onClick={()=>{dislike(); calc()}}><img src={down} /></div>
          <div className='thumbButtom'>Vote Now</div>
        </div>
      </div>
      <ResultSection results={results} setVote={setVote} vote={vote}/>
    </div>
  );
};

export default Card;
