import React from "react";
import up from "../../images/up.png";
import down from "../../images/down.png";

const ResultSection = ({results, setVote, vote}) =>{
  return (
    <>
    <div className='vote'>
      {results.data2!=100 && <div className='option1' style={{flex: 'auto', width: results.data1+'%'}}>
        <img src={up} />
        <div className='results'>{results.data1}%</div>
      </div>}
      {results.data1!=100 && <div className='option2' style={{flex: 'auto', width: results.data2+'%'}}>
        <div className='results'>{results.data2}%</div>
        <img src={down} />
      </div>}
    </div>
    {
      vote && 
      <div className='again' onClick={()=>setVote(false)}>
        Vote Again
      </div>
    }
    </>
  )
}

export default ResultSection