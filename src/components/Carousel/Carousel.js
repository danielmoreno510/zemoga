import React from "react";
import "./styles.css";

const Carousel = () => (
  <div className='fondo'>
    <div className='closingIn'>
      <div className='closingInSection1'>
        <div className='closingInText1'>CLOSING IN</div>
      </div>
      <div className='closingInSection2'>
        <div className='closingInText2'>22 days</div>
      </div>
    </div>
  </div>
);

export default Carousel;
