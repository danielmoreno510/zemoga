import React, { useState } from "react";
import up from "../../images/up.png";
import down from "../../images/down.png";
import "./styles.css";
import ResultSection from '../ResultSection'
import { percentage } from '../../Utils/percentageUtil'

const Card = ({ like, dislike, data }) => {
  const [vote, setVote] = useState(false);
  const [results, setResults] = useState(false);
  return (
    <div className='card'>
      <div className='cardContainer'>
        <div className='textInitial'>What's your opinion on</div>
        <div className='textTitle'>Pope Francis?</div>
        <div className='textInfo'>
          He's talking tough on clergy sexual abuse, but is he just another
          papal pervert protector? (thumbs down) or a true pedophile punishing
          pontiff? (thumbs up)
        </div>
        <div className='textLink'>More information</div>
        <div className='textSubtitle'>What's Your Verdict?</div>
      </div>
      {vote ? (
        <ResultSection results={results} setVote={setVote} vote={vote} />
      ) : (
        <VoteSection
          setVote={setVote}
          like={like}
          dislike={dislike}
          data={data}
          setResults={setResults}
        />
      )}
    </div>
  );
};

const VoteSection = ({ setVote, like, dislike, data, setResults }) => {
  const calc = () => {
    setVote(true);
    setResults(percentage(data.likes,data.dislikes))
  };
  return (
    <div className='vote'>
      <div className='option1' onClick={() => { like(); calc(); }}>
        <img src={up} />
      </div>
      <div className='option2' onClick={() => { dislike(); calc(); }} >
        <img src={down} />
      </div>
    </div>
  );
};

export default Card;
