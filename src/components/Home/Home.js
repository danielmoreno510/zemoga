import React from "react";
import PropTypes from 'prop-types'
import Card from "./Card";
import PrincipalCard from "../PrincipalCard";
import "./styles.css";

const Home = ({items, likePrincipal, like, data}) => {
  return (
    <div className="homeContainer">
      <Card like={()=>likePrincipal('like')}  dislike={()=>likePrincipal('dislike')} data={data} />
      <div className="cardLarge">
        <div className="cardLargeItem1">
          <div className="cardLargeItem1Section1"> Speak out. Be heard</div>
          <div className="cardLargeItem1Section2">Be Counted</div>
        </div>
        <div className="cardLargeItem2">
          Rule of Thumb is a crowd sourced court of public opinion where anyone
          and everyone can speak out and speak freely. It's easy: You share your
          opinion, we analyze and put the data in puclic report.
        </div>
      </div>
      <div className="voteSection">Votes</div>
      <div className="spaceCards">
        {items.map((item, i) => (
          <PrincipalCard data={item} key={i} like={()=>like(('like'), item.id)} dislike={()=>like(('dislike'), item.id)} />
          ))}
      </div>
    </div>
  );
};

Home.propTypes = {
  like: PropTypes.func.isRequired
}

export default Home;
