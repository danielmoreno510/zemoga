import React from "react";
import { Link, Route } from "react-router-dom";
import search from '../../images/search.png';
import PastTrials from '../PastTrials';
import LogInSignUp from '../LogInSignUp';
import HowItWorks from '../HowItWorks';
import "./styles.css";

const Header = ({change}) => (
  <div className='headerContainer'>
    <div className='headerSections'>
      <div className='section1'>
        <Link className='headerOptions' style={{textDecoration: 'none', color: 'white'}} to="/" onClick={()=>change(true)}>Rule of Thumb.</Link>
      </div>
      <div className='section2'>
        <Link className='headerOptions' style={{textDecoration: 'none', color: 'white'}} to="/pastTrials" onClick={()=>change(false)}>Past Trials</Link>
        <Link className='headerOptions' style={{textDecoration: 'none', color: 'white'}} to="/howItWorks" onClick={()=>change(false)}>How It Works</Link>
        <Link className='headerOptions' style={{textDecoration: 'none', color: 'white'}} to="/logInSignUp" onClick={()=>change(false)}>Log In / Sign Up</Link>
        <img src={search} />
      </div>
    </div>
    <Route path="/pastTrials" component={PastTrials} />
    <Route path="/howItWorks" component={HowItWorks} />
    <Route path="/logInSignUp" component={LogInSignUp} />
  </div>
);

export default Header;
