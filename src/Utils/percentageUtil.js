export const percentage = (data1, data2) => {
  const total=data1+data2
  const totalData1=Math.round(((data1*100)/total))
  const totalData2=Math.round(100-totalData1)
  return {'data1':totalData1,'data2':totalData2}
};
