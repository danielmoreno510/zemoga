Prueba Tecnica Zemoga

## Proyecto

Este proyecto muestra una pagina que permite realizar votaciones, cada voto es guardado en el estado de la aplicacion, si navega a otro link, y vueve al home los votos seguiran guardados en el estado, solo seran reestablecidos cuando se actualice la pagina

## Tecnologias

Ya que la vacante es para react js, el proyecto se realizo en esta tecnologia, ademas se implemento redux para el estado de la aplicacion; la aplicacion es responsive, puede verla desde cualquier dispositivo.

## Estructura

Este proyecto fue desarrollado mediante componentes funcionales para la reutilizacion de codigo.
En las diferentes campetas encontrara el codigo para cada funcionalidas, containers, componentes, state (redux), Utils

## Link

http://zemoga.000webhostapp.com/